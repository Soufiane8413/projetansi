#!/bin/bash
while IFS=';' read -r user_ip motdepasse; 
do
	echo "$motdepasse" | sshpass ssh-copy-id -f -i /home/jenkinbot/.ssh/id_rsa $user_ip -o StrictHostKeyChecking=no
done < listofservers
